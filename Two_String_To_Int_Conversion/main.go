package Two_String_To_Int_Conversion

import "errors"

func atoi(s string) (int, error) {

	str := []rune(s)
	length := len(s)
	intMax, intMin := (1<<31)-1, 1<<31

	var i int
	for i < length && str[i] == ' '{
		i++
	}

	var sign int = 1
	if i < length && (str[i] == '-' || str[i] == '+'){
		if str[i] == '-' {
			sign = -1
		}
		i++
	}

	var res int

	for i < length && str[i] >= '0' && str[i] <= '9' {
		digit := int(str[i] - '0')

		if (sign > 0 && res > ((intMax - digit) / 10)) || (sign < 0 && res > ((intMin - digit) / 10)) {
			if sign == 1 {
				return intMax, errors.New("1")
			} else {
				return -intMin, errors.New("2")
			}
		}
		res = res * 10 + digit
		i++
	}

	if res == 0 {
		return sign * res, errors.New("string contains non-int chatacters")
	}
	return sign * res, nil
}