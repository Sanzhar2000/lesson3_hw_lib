package Six_Rune_By_Index

import (
	"errors"
	"fmt"
	"log"
)


func runeByIndex(s *string, i *int) (a rune, err error) {

	defer func() {
		if r := recover(); r != nil {
			log.Printf("panic recovered: %v\n", r)
			err = errors.New(fmt.Sprintf("%v", r))
			a = 0
		}
	} ()

	str := []rune(*s)
	a = str[*i]

	return
}