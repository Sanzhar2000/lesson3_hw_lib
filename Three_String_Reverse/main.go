package Three_String_Reverse

func reverse(s string) string {

	r := []rune(s)
	var res []rune

	for i := len(r) - 1; i >= 0; i-- {
		res = append(res, r[i])
	}

	return string(res)
}