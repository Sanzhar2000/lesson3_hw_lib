package Four_Imports_Sorting_In_File

import (
	"bufio"
	"log"
	"os"
	"sort"
)

func sortImports(fPath string) {

	file, err := os.Open(fPath)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var strings [] string
	for scanner.Scan() {
		strings = append(strings, scanner.Text())
	}
	err = file.Close()
	if err != nil {
		return
	}


	var imports []string
	var i int

	var fileBeforeImports []string
	var fileAfterImports []string

	for _, eachline := range strings {
		// i = 2 after imports
		// i = 1 import
		// i = 0 before imports
		if i == 2 {
			fileAfterImports = append(fileAfterImports, eachline)
		}

		if eachline == ")" && i == 1 {
			fileAfterImports = append(fileAfterImports, eachline)
			i = 2
		}

		if i == 1 {
			imports = append(imports, eachline)
		}

		if eachline == "import (" && i == 0 {
			fileBeforeImports = append(fileBeforeImports, eachline)
			i = 1
		}

		if i == 0 {
			fileBeforeImports = append(fileBeforeImports, eachline)
		}
	}

	sort.Strings(imports)

	var res []string
	res = append(res, fileBeforeImports...)
	res = append(res, imports...)
	res = append(res, fileAfterImports...)

	err = os.Remove(fPath)
	if err != nil {
		log.Fatalf("failed deleting file: %s", err)
	}

	file, err = os.OpenFile(fPath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range res {
		_, _ = datawriter.WriteString(data + "\n")
	}

	err = datawriter.Flush()
	if err != nil {
		return
	}

	err = file.Close()
	if err != nil {
		return
	}
}