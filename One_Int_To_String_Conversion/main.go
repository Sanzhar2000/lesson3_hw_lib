package One_Int_To_String_Conversion

func itoa(n int) string {

	arr := [10]string{"0","1","2","3","4","5","6","7","8","9"}
	var ans string
	var minus string

	if n < 0 {
		n *= -1
		minus = "-"
	}

	if n <= 9 {
		return minus + arr[n]
	}

	for ; n > 0; {
		piece := n % 10
		n = (n - piece) / 10
		ans = arr[piece] + ans
	}

	return minus + ans
}